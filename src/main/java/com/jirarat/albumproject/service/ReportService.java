/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirarat.albumproject.service;

import com.jirarat.albumproject.dao.SaleDao;
import com.jirarat.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author User
 */
public class ReportService {
    public List<ReportSale> getReportSaleByDay(){
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
        
    }
    public List<ReportSale> getReportSaleByMonth(int year){
        SaleDao dao = new SaleDao();
       return dao.getMonthReport(year);
        
    }
}
